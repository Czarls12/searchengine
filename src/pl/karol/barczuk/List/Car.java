package pl.karol.barczuk.List;

public class Car implements Comparable<Car> {

    String mark;
    String model;

    public Car(String mark, String model) {
        this.mark = mark;
        this.model = model;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int compareTo(Car o) {
//        int result = -1;
//        if (getModel() == o.getModel()
//                && getMark() == o.getMark()) {
//            result = 0;
//        }
        int result = getMark().compareTo(o.getMark());

        if(result == 0) {
            return getModel().compareTo(o.getModel());
        }
        else {
            return result;
        }
    }
}
