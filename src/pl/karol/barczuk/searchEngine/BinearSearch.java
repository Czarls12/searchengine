package pl.karol.barczuk.searchEngine;

import pl.karol.barczuk.List.Car;

import java.util.List;

public class BinearSearch<T extends Comparable<T>> extends Search<T> {

    List<T> savePostionList;

    int temp = 0;
    int divide = 1;

    @Override
    public int doSearch(List<T> list, T searchElement) {

        List<T> tempList = list;


        if (tempList.size() > 0) {
            int listSize;
            if (tempList.size() % 2 != 0) {
                listSize = (tempList.size() + divide) / 2;
            }else {
                listSize = tempList.size() / 2;
            }

            if (list.get(listSize).compareTo(searchElement) == 0){
               return listSize + temp;
            }


            else if (list.get(listSize).compareTo(searchElement) > 0) {

                this.divide = 1;
                for (int i = tempList.size() - 1; i >= listSize ; i--) {
                    tempList.remove(i);
                }
                //List<T> secountTempList = tempList;
                //int tempsize = secountTempList.size();
                return doSearch(tempList, searchElement);
            }


            else if (list.get(listSize).compareTo(searchElement) < 0) {

                this.temp += listSize;
                this.divide = -1;

                for (int i = 0; i <= listSize; i++) {

                    tempList.remove(i);
                }
                List<T> secountTempList = tempList;
                //int tempsize = secountTempList.size();
                return doSearch(secountTempList, searchElement);
            }
        }
        return -1;
    }
}
