package pl.karol.barczuk;

import pl.karol.barczuk.List.Car;
import pl.karol.barczuk.searchEngine.BinearSearch;
import pl.karol.barczuk.searchEngine.LinearSearch;
import pl.karol.barczuk.searchEngine.Search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainClass {

    public static void main(String[] args) {

        Car car = new Car("Fiat", "126p");
        Car car2 = new Car("Audi", "A4");

        List<Car> cars = new ArrayList<>();

        cars.add(new Car("Fiat", "126p"));
        cars.add(new Car("Audi", "A8"));
        cars.add(new Car("Audi", "A6"));
        cars.add(new Car("Nissan", "GTR"));
        cars.add(new Car("Audi", "RS4"));
        cars.add(new Car("Audi", "A4"));


        Search<Car> searchEngine = new LinearSearch<>();
        Search<Car> searchEngine2 = new BinearSearch<>();




        System.out.println("Element znajduje sie na pozycji: " + searchEngine.doSearch(cars, car));

        for (Car xl : cars) {
            System.out.println(xl.getMark() +" " + xl.getModel());
        }

        System.out.println("sort");
        Collections.sort(cars);

        for (Car xl : cars) {
            System.out.println(xl.getMark() +" " + xl.getModel());
        }

        System.out.println("Element znajduje sie na pozycji: " + searchEngine.doSearch(cars, car2));

        System.out.println("Element znajduje sie na pozycji: " + searchEngine2.doSearch(cars, car));
/*
        String x = "A4";
        String x2 = "A6";

        x.compareTo(x2);
        System.out.println(x.compareTo(x2));

        for (Car xl : cars) {
            System.out.println(xl.getMark() +" " + xl.getModel());
        }

        System.out.println("sort");

        for (Car xl : cars) {
            System.out.println(xl.getMark() +" " + xl.getModel());
        }

        System.out.println();

*/




    }
}
